package model;

import java.util.LinkedHashMap;
import java.util.Map;
import ui.ShowAllBooksJframe;
import ui.TipsJframe;

/**
 * 
 * @author �ƽ�
 *
 */
public class Library {
	private static Map<String, Book> map = new LinkedHashMap<>();
	private static ShowAllBooksJframe showAllBooksJframe = new ShowAllBooksJframe();

	public static ShowAllBooksJframe getShowAllBooksJframe() {
		return showAllBooksJframe;
	}

	public static Map<String, Book> getMap() {
		return map;
	}

	public static void showBooks() {
		showAllBooksJframe.setVisible(true);
	}

	static {
		Book b1 = new Book("1", "����������", "5", "���ա�������");
		Book b2 = new Book("2", "���ص�", "5", "���ա�������");
		Book b3 = new Book("3", "�����ش����Ķ�Ů", "5", "���ա�������");
		Book b4 = new Book("4", "ͯ��", "5", "�߶���");
		Book b5 = new Book("5", "�����", "5", "������������");
		Book b6 = new Book("6", "���μ�", "5", "��ж�");
		Book b7 = new Book("7", "��¥��", "5", "��ѩ��");

		map.put(b1.getId(), b1);
		map.put(b1.getName(), b1);
		map.put(b2.getId(), b2);
		map.put(b2.getName(), b2);
		map.put(b3.getId(), b3);
		map.put(b3.getName(), b3);
		map.put(b4.getId(), b4);
		map.put(b4.getName(), b4);
		map.put(b5.getId(), b5);
		map.put(b5.getName(), b5);
		map.put(b6.getId(), b6);
		map.put(b6.getName(), b6);
		map.put(b7.getId(), b7);
		map.put(b7.getName(), b7);
	}

	public static String searchBooks(String idOrName) {
		String string = null;
		if (Library.getMap().get(idOrName) != null) {
			TipsJframe tipsJframe = new TipsJframe(idOrName);
			String number = "0";
			if (Library.getMap().get(idOrName).getNumber().equals(number)) {
				string = "��ͼ���ѱ���գ�";
			}
			string = "�����ɹ���";
			tipsJframe.setVisible(true);
		} else {
			string = "ͼ�����δ�ҵ����飡";
		}
		return string;
	}
}
