package model;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import ui.MyBooksList;
import ui.ShowAllBooksJframe;
import ui.TipsJframe;
import util.Main;

/**
 * 
 * @author 黄健
 *
 */
public class Reader {
	private static MyBooksList myBooksList = new MyBooksList();

	public static MyBooksList getMyBooksList() {
		return myBooksList;
	}

	public static void setMyBooksList(MyBooksList myBooksList) {
		Reader.myBooksList = myBooksList;
	}

	public static void borrowBooks(String idOrName) {
		Book book = Library.getMap().get(idOrName);
		String number = book.getNumber();
		Integer number1 = Integer.parseInt(number);
		number1--;
		Library.getMap().get(idOrName).setNumber(number1.toString());
		int rowCount = Library.getShowAllBooksJframe().getjTable1().getRowCount();
		for (int i = 0; i < rowCount; i++) {
			if (Library.getShowAllBooksJframe().getjTable1().getValueAt(i, 0).equals(idOrName)
					|| Library.getShowAllBooksJframe().getjTable1().getValueAt(i, 1).equals(idOrName)) {
				Library.getShowAllBooksJframe().getjTable1().setValueAt(number1.toString(), i, 3);
				break;
			}
		}
		
		DefaultTableModel model = (DefaultTableModel) Library.getShowAllBooksJframe().getjTable1().getModel();
		for (int i = 0; i < Library.getShowAllBooksJframe().getjTable1().getRowCount(); i++) {
			if (Library.getShowAllBooksJframe().getjTable1().getValueAt(i, 3).equals("0")) {
				model.removeRow(i);
				i--;
			}
		}

		String[] st = new String[5];
		st[0] = TipsJframe.getjTextField1().getText();
		st[1] = TipsJframe.getjTextField2().getText();
		st[2] = TipsJframe.getjTextField3().getText();
		st[3] = Main.getBorrowTime();
		st[4] = Main.getReturnTime();
		DefaultTableModel model1 = (DefaultTableModel) Reader.myBooksList.getjTable1().getModel();
		model1.addRow(st);
	}

	public static String returnBooks() {
		DefaultTableModel model = (DefaultTableModel) myBooksList.getjTable1().getModel();
		ShowAllBooksJframe showAllBooksJframe = Library.getShowAllBooksJframe();
		DefaultTableModel model2 = (DefaultTableModel) showAllBooksJframe.getjTable1().getModel();
		int flag = 0;
		String string = null;
		for (int i = 0; i < myBooksList.getjTable1().getRowCount(); i++) {
			if (myBooksList.getjTable1().getValueAt(i, 5) != null) {
				if ((Boolean) myBooksList.getjTable1().getValueAt(i, 5) == true) {
					string = "已选中勾选框！";
					flag = 1;
					Book book = Library.getMap().get(myBooksList.getjTable1().getValueAt(i, 0));
					if (searchRow((String) myBooksList.getjTable1().getValueAt(i, 0), showAllBooksJframe) != -1) {
						Integer number = Integer.parseInt((String) model2.getValueAt(
								searchRow((String) myBooksList.getjTable1().getValueAt(i, 0), showAllBooksJframe), 3));
						number++;
						book.setNumber(number.toString());
						model2.setValueAt(number.toString(),
								searchRow((String) myBooksList.getjTable1().getValueAt(i, 0), showAllBooksJframe), 3);
					} else {
						book.setNumber("1");
						String[] st = new String[4];
						st[0] = book.getId();
						st[1] = book.getName();
						st[2] = book.getAuthor();
						st[3] = "1";
						model2.addRow(st);
					}

					model.removeRow(i);
					i--;
				}
			}

		}

		if (flag == 0) {
			string = "请选择勾选框！";
		}
		return string;
	}

	public static int searchRow(String id, ShowAllBooksJframe showAllBooksJframe) {
		int result = -1;
		TableModel model = showAllBooksJframe.getjTable1().getModel();
		int rowCount = model.getRowCount();
		for (int j = 0; j < rowCount; j++) {
			if (model.getValueAt(j, 0).equals(id)) {
				result = j;
				break;
			}
		}
		return result;
	}

	public static String renewBooks() {
		String string = null;
		int rowCount = myBooksList.getjTable1().getRowCount();
		int flag = 0;
		for (int i = 0; i < rowCount; i++) {
			if (myBooksList.getjTable1().getValueAt(i, 5) != null) {
				if ((Boolean) myBooksList.getjTable1().getValueAt(i, 5) == true) {
					flag = 1;
					string = "已选中勾选框！";
					myBooksList.getjTable1().setValueAt(Main.getBorrowTime(), i, 3);
					myBooksList.getjTable1().setValueAt(Main.getReturnTime(), i, 4);
				}
			}

		}
		if (flag == 0) {
			string = "请选择勾选框！";
		}
		return string;
	}
}
