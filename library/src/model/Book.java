package model;

/**
 * 
 * @author 黄健
 *
 */
public class Book {
	/** 图书编号 */
	private String id;
	/** 图书名称 */
	private String name;
	/** 图书的数量 */
	private String number;
	/** 图书作者 */
	private String author;

	public Book(String id, String name, String number, String author) {
		super();
		this.id = id;
		this.name = name;
		this.number = number;
		this.author = author;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNumber() {
		return number;
	}

	public String getAuthor() {
		return author;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
