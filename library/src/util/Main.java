package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import ui.LibraryMainJframe;
import ui.LoginOrRegisterJframe;

/**
 * 
 * @author �ƽ�
 *
 */
public class Main {
	public static void main(String[] args) throws IOException {
		LoginOrRegisterJframe.main(args);
	}

	public static String getBorrowTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(date);

	}

	public static String getReturnTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(" HH:mm");
		int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));
		int month = Integer.parseInt(new SimpleDateFormat("MM").format(date)) + 1;
		int day = Integer.parseInt(new SimpleDateFormat("dd").format(date));
		int monthCount = 13;
		int monthCount1 = 2;
		int dayNumber = 28;
		if (month == monthCount) {
			month = 1;
			year += 1;
		}

		if (day > dayNumber) {
			boolean flag = false;
			flag = year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
			boolean flag2 = false;
			flag2 = (month == 4 || month == 6 || month == 9 || month == 11) && day == 31;
			if (month == monthCount1) {
				if (flag) {
					day = 29;
				} else {
					day = 28;
				}
			} else if (flag2) {
				day = 30;
			}
		}
		String y = year + "";
		String m = "";
		String d = "";
		int monthCount2 = 9;
		int dayCount = 10;
		if (month <= monthCount2) {
			m = "0" + month;
		} else {
			m = month + "";
		}

		if (day < dayCount) {
			d = "0" + day;
		} else {
			d = day + "";
		}

		return y + "-" + m + "-" + d + format.format(date);
	}
	
	public static String login(String accountNumber, String password) {
		String string = null;
		if ("".equals(password)) {
			string = "���벻��Ϊ�գ�";
			return string;
		}
		try {
			String name = "reader.txt";
			FileReader fr = new FileReader(name);
			BufferedReader bf = new BufferedReader(fr);
			String str;
			int flag = 0;
			// ���ж�ȡ�ַ���
			while ((str = bf.readLine()) != null) {
				if ("".equals(str)) {
					break;
				}
				String[] st = str.split("\\s+");
				if (st.length == 0) {
					break;
				}
				if (accountNumber.equals(st[0]) && password.equals(st[1])) {
					LibraryMainJframe mainFrame = new LibraryMainJframe();
					mainFrame.setVisible(true);
					flag = 1;
					string = "��½�ɹ���";
					break;
				} else if (accountNumber.equals(st[0]) && !password.equals(st[1])) {
					flag = 1;
					string = "�˺Ż��������";
					break;
				}
			}

			if (flag == 0) {
				string = "���˺Ų����ڣ�";
			}

			bf.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string;
	}

	public static String register(String accountNumber, String password) {
		String string = null;
		if ("".equals(password)) {
			string = "���벻��Ϊ�գ�";
			return string;
		}
		try {
			String name = "reader.txt";
			FileReader fr = new FileReader(name);
			BufferedReader bf = new BufferedReader(fr);
			String str = null;
			int flag = 0;
			// ���ж�ȡ�ַ���
			while ((str = bf.readLine()) != null) {
				if ("".equals(str)) {
					break;
				}
				String[] st = str.split("\\s+");
				if (st.length == 0) {
					break;
				}
				if (accountNumber.equals(st[0])) {
					string = "�˺��Ѵ��ڣ�";
					flag = 1;
					break;
				}
			}

			if (flag == 0) {
				File file = new File(name);
				Writer out = new FileWriter(file, true);
				String strReader = accountNumber + " " + password;
				out.write(strReader + "\n");
				out.close();
				string = "ע��ɹ���";
			}
			bf.close();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string;
	}
}
